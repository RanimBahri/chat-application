import React from "react";

import ScrollToBottom from "react-scroll-to-bottom";

import SingleMessageComponent from "../SingleMessageComponent";

import "./_MessagesComponent.scss";

const Messages = ({ messages, name }) => (
  <ScrollToBottom className="messages">
    {messages.map((message, i) => (
      <div key={i}>
        <SingleMessageComponent
          message={message}
          name={name}
          renderName={i === 0 || messages[i - 1].user !== message.user}
        />
      </div>
    ))}
  </ScrollToBottom>
);

export default Messages;
