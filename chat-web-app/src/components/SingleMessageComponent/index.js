import React from "react";

import "./_SingleMessageComponent.scss";

import ReactEmoji from "react-emoji";

const SingleMessageComponent = ({
  message: { payload, user },
  name,
  renderName,
}) => {
  let isSentByCurrentUser = false;

  const trimmedName = name.trim().toLowerCase();

  if (user === trimmedName) {
    isSentByCurrentUser = true;
  }

  if (user === "admin")
    return (
      <div className="admin-message-container">
        <span className="admin-message">{ReactEmoji.emojify(payload)}</span>
      </div>
    );

  return (
    <div
      style={{
        display: "flex",
        wordBreak: "break-word",
        marginTop: renderName ? 20 : 0,
      }}
    >
      {renderName && (
        <div className="user-avatar">
          <span>{user[0]}</span>
        </div>
      )}

      <div className="messageBox" style={{ marginLeft: renderName ? 0 : 50 }}>
        {renderName && (
          <div className="name-container">
            <p className="sentText">
              {isSentByCurrentUser ? trimmedName : user}
            </p>
          </div>
        )}
        <div>
          <p className="messageText">{ReactEmoji.emojify(payload)}</p>
        </div>
      </div>
    </div>
  );
};

export default SingleMessageComponent;
