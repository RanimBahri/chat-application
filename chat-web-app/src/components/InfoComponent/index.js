import React from "react";

import "./_InfoComponent.scss";

const InfoComponent = ({ room }) => (
  <div className="infoBar">
    <div className="leftInnerContainer">
      <img className="onlineIcon" src="/onlineIcon.png" alt="online icon" />
      <h3>{room}</h3>
    </div>
    <div className="rightInnerContainer">
      <a href="/">
        <img src="/closeIcon.png" alt="close icon" />
      </a>
    </div>
  </div>
);

export default InfoComponent;
