import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { Provider } from "react-redux";

import { PersistGate } from "redux-persist/integration/react";

import { myStore, persistor } from "./_redux/store";
import JoinPage from "./pages/JoinPage";
import ChatPage from "./pages/ChatPage";

ReactDOM.render(
  <Provider store={myStore}>
    <PersistGate loading={null} persistor={persistor}>
      <Router>
        <Route path="/" exact component={JoinPage} />
        <Route path="/chat" component={ChatPage} />
      </Router>{" "}
    </PersistGate>
  </Provider>,
  document.getElementById("root")
);
