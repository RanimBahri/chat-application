import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import io from "socket.io-client";
import { SERVER_API } from "../../api";

import InfoComponent from "../../components/InfoComponent";
import MessagesComponent from "../../components/MessagesComponent";
import ChatForm from "../../containers/ChatForm";

import "./_ChatPage.scss";

let socket;

const ChatPage = () => {
  const [name, setName] = useState("");
  const [room, setRoom] = useState("");
  const [users, setUsers] = useState("");
  const [message, setMessage] = useState("");
  const [messages, setMessages] = useState([]);

  const { currentUser } = useSelector((state) => state.userReducer);

  useEffect(() => {
    const { name, room } = currentUser;
    console.log({ currentUser });
    socket = io(SERVER_API);

    setRoom(room);
    setName(name);

    socket.emit("join", { name, room }, (error) => {
      if (error) {
        alert(error);
      }
    });

    return () => {
      socket.emit("disconnected");
      socket.off();
    };
  }, [currentUser]);

  useEffect(() => {
    socket.on("message", (message) => {
      setMessages((messages) => [...messages, message]);
    });

    socket.on("roomData", ({ users }) => {
      setUsers(users);
    });
  }, []);

  const sendMessage = (event) => {
    event.preventDefault();

    if (message) {
      socket.emit("sendMessage", message, () => setMessage(""));
    }
  };

  return (
    <div className="outerContainer">
      <div className="container">
        <InfoComponent room={room} />
        <MessagesComponent messages={messages} name={name} />
        <ChatForm
          message={message}
          setMessage={setMessage}
          sendMessage={sendMessage}
        />
      </div>
    </div>
  );
};

export default ChatPage;
