import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import { joinUser } from "../../_redux/actions";
import "./_JoinPage.scss";

const JoinPage = () => {
  const [name, setName] = useState("");
  const [room, setRoom] = useState("");

  const dispatch = useDispatch();

  const signIn = () => {
    dispatch(joinUser({ name, room }));
  };

  return (
    <div className="joinOuterContainer">
      <form className="joinInnerContainer">
        <h1 className="heading">Join</h1>
        <div>
          <input
            placeholder="Name"
            className="joinInput"
            type="text"
            onChange={(event) => setName(event.target.value)}
          />
        </div>
        <div>
          <input
            placeholder="Room"
            className="joinInput mt-20"
            type="text"
            onChange={(event) => setRoom(event.target.value)}
          />
        </div>
        <Link onClick={signIn} to={"/chat"}>
          <button className={"button mt-20"} type="submit">
            Join
          </button>
        </Link>
      </form>
    </div>
  );
};

export default JoinPage;
