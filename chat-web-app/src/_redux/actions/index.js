export const joinUser = (data) => ({
  type: "JOIN_USER_SUCCESS",
  payload: data,
});
