import { JOIN_USER_SUCCESS, JOIN_USER_FAILURE } from "../types/user";

const userReducer = (state = {}, action) => {
  const { type, payload, error } = action;
  switch (type) {
    case JOIN_USER_SUCCESS:
      return { ...state, currentUser: payload };
    case JOIN_USER_FAILURE:
      return { ...state, error };
    default:
      return state;
  }
};

export default userReducer;
