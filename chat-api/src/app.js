const http = require("http");
const express = require("express");
const router = require("./routes");

const { getUser, addUser, removeUser, getUsersInRoom } = require("./services");

const PORT = process.env.PORT || 3001;
const app = express();
const server = http.createServer(app);
const io = require("socket.io")(server, {
  cors: {
    origin: "*",
  },
});

io.on("connection", (socket) => {
  socket.on("join", ({ name, room }, callback) => {
    const { error, user } = addUser({ id: socket.id, name, room });

    if (error) return callback(error);

    socket.join(user.room);

    // admin generated message
    socket.emit("message", {
      user: "admin",
      payload: `${user.name}, You are in room ${user.room}.`,
    });
    socket.broadcast
      .to(user.room)
      .emit("message", { user: "admin", payload: `${user.name} is here!` });

    io.to(user.room).emit("roomData", {
      room: user.room,
      users: getUsersInRoom(user.room),
    });

    callback();
  });

  // user message
  socket.on("sendMessage", (message, callback) => {
    const user = getUser(socket.id);
    if (user)
      io.to(user.room).emit("message", { user: user.name, payload: message });
    callback();
  });

  socket.on("disconnect", () => {
    const user = removeUser(socket.id);

    if (user) {
      io.to(user.room).emit("message", {
        user: "admin",
        payload: `${user.name} has left.`,
      });
      io.to(user.room).emit("roomData", {
        room: user.room,
        users: getUsersInRoom(user.room),
      });
    }
  });
});

app.use(router);

server.listen(PORT, () => console.log(`Server has started on ${PORT}`));
